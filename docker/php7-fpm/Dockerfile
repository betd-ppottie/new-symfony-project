FROM php:fpm

LABEL maintenainer="Pierre Pottié <pierre.pottie@gmail.com>"

ARG PROJECT_NAME

ENV PROJECT_NAME=${PROJECT_NAME}


ENV LC_ALL=${LANG}
ENV LANG=${LANG}
ENV LANGUAGE=${LANGUAGE}

RUN systemMods=" \
        apt-utils \
        openssl \
        acl \
        graphicsmagick \
        libssl-dev \
        libmcrypt-dev \
        curl \
        wget \
        libicu-dev \
        libpq-dev \
        zip \
        libxml2-dev \
        unzip \
        locales \
        locales-all \
    " \
    && apt-get update \
    && apt-get install -y $systemMods \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

# Set timezone
RUN rm /etc/localtime \
    && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN "date"


# Install APCu 
RUN pecl install apcu && \
 echo "extension=apcu.so" > /usr/local/etc/php/conf.d/apcu.ini


# Install the available extensions
RUN docker-php-ext-install pdo  intl zip soap mcrypt opcache

# POSTGRES
RUN docker-php-ext-install pdo_pgsql

# Enable apc
RUN echo "apc.enable_cli = On" >> /usr/local/etc/php/conf.d/docker-php-ext-apcu.ini



RUN pecl install -o -f xdebug \
    && rm -rf /tmp/pear

RUN docker-php-ext-enable xdebug
RUN echo "error_reporting = E_ALL" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "display_startup_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "display_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_connect_back=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.idekey=\"PHPSTORM\"" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_port=9004" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# set recommended opcache PHP.ini settings # see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
} > /usr/local/etc/php/conf.d/opcache-recommended.ini
# set recommended apcu PHP.ini settings # see https://secure.php.net/manual/en/apcu.configuration.php
RUN { \
        echo 'apc.shm_segments=1'; \
        echo 'apc.shm_size=256M'; \
        echo 'apc.num_files_hint=7000'; \
        echo 'apc.user_entries_hint=4096'; \
        echo 'apc.ttl=7200'; \
        echo 'apc.user_ttl=7200'; \
        echo 'apc.gc_ttl=3600'; \
        echo 'apc.max_file_size=1M'; \
        echo 'apc.stat=1'; \
} > /usr/local/etc/php/conf.d/apcu-recommended.ini

RUN sed -i -e 's/listen.*/listen = 0.0.0.0:9000/' /usr/local/etc/php-fpm.conf

# for linux
RUN usermod -u 1000 www-data

# for mac
#RUN usermod -u 501 www-data

# Expose ports
EXPOSE 9000
EXPOSE 9004

WORKDIR /var/www/
