
# make a symfony project a project under docker


 first you need to copy the .env_dist to .env and custom it

 exemple configuration pour dockerisation d'un projet existant
 fichier `.env`
 ```
 PROJECT_NAME=test

# LANG (on build)
LANG=fr_FR.UTF-8
LANGUAGE=fr_FR

#ENVIRONMENT
ENV=dev

# Symfony Project
SYMFONY_APP_PATH=../symfony/test_create

# PROJECT INSTALLATION
COMPOSER_INSTALL_PATH=test/
NPM_INSTALL_PATH=NO_NPM


# Nginx
NGINX_SERVER_NAME=test.dev

DB_ROOT_PASSWORD=your_root_password
DB_DATABASE=your_database_name
DB_USER=your_user_name
DB_PASSWORD=your_user_pass

 ```

 lancer l'installation 
 ```bash
 $ make install
 ```
Le projet `PROJECT_NAME` (ex:"test") est maintenant dockerisé
aller dans le repertoire du projet puis lancer :
```bash
$ make install 
```
les images seront créées et lancées.
les commandes `composer install` et `npm install` seront lancées si définies dans le fichier `.env`

```bash
$ docker ps
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                              NAMES
45e91fe02695        nginx:alpine         "nginx -g 'daemon ..."   23 minutes ago      Up 23 minutes       0.0.0.0:80->80/tcp                 test_nginx
c95203376706        testcreate_php-fpm   "docker-php-entryp..."   23 minutes ago      Up 23 minutes       0.0.0.0:9000->9000/tcp, 9004/tcp   test_app
43d070b12063        postgres             "docker-entrypoint..."   2 hours ago         Up About an hour    0.0.0.0:5432->5432/tcp             test_pgsql
```


Il vous reste à ajouter dans votre fichier /etc/hosts
```
127.0.0.1 test.dev
```