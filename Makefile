UID:=$(shell id -u)
GID:=$(shell id -g)
CC:=@docker-compose
PROJECT_NGINX_CONF:=docker/nginx/conf/your-project.conf

include .env
export

install: install_docker_environnement


install_docker_environnement:
	@echo "copy files"
	cp -R .env .dockerignore docker README.docker ${SYMFONY_APP_PATH}
	@sed -i "s/^SYMFONY_APP_PATH=.*/SYMFONY_APP_PATH=\.\//" ${SYMFONY_APP_PATH}/.env
	cp ${SYMFONY_APP_PATH}/.env ${SYMFONY_APP_PATH}/.env_dist
	@echo "custom files"
	cp ./docker/docker-compose.yml.* ${SYMFONY_APP_PATH}/
	@echo "create logs directory ${SYMFONY_APP_PATH}/var/logs"
	mkdir -p ${SYMFONY_APP_PATH}/var/logs
	@echo "install Makefile for project"
	cp Makefile.docker ${SYMFONY_APP_PATH}/Makefile
	@echo "create of the project's nginx configuration file"
	cp ./${PROJECT_NGINX_CONF} ${SYMFONY_APP_PATH}/${PROJECT_NGINX_CONF}
	@sed -i "s/\[PROJECT_NAME\]/${PROJECT_NAME}/g" ${SYMFONY_APP_PATH}/${PROJECT_NGINX_CONF}
	@sed -i "s/\[PROJECT_NAME\]/${PROJECT_NAME}/g" ${SYMFONY_APP_PATH}/${PROJECT_NGINX_CONF}
	@sed -i "s/\[NGINX_SERVER_NAME\]/${NGINX_SERVER_NAME}/g" ${SYMFONY_APP_PATH}/${PROJECT_NGINX_CONF}
	mv ${SYMFONY_APP_PATH}/${PROJECT_NGINX_CONF} ${SYMFONY_APP_PATH}/docker/nginx/conf/${PROJECT_NAME}.conf
	@echo "${SYMFONY_APP_PATH}/docker/nginx/${PROJECT_NAME}.conf created"
